import os
from flask import Flask, request
from flask_socketio import SocketIO, emit
from apps.twitter.controllers.api import Api

os.environ.setdefault("CONFIG_PATH", "apps/twitter")
os.environ.setdefault("CONFIG_FILE", "config.conf")
os.environ.setdefault("VIEW_NAME", 'twitter')
os.environ.setdefault("MODE_NAME", '')

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, cors_allowed_origins='*', ping_timeout=30000, async_mode=None)

api = Api()

@app.route("/")
def index():
    return "Hi, Welcome!"

@socketio.on('connect')
def connect():
    print 'Client connecting', request.remote_addr

@socketio.on("set_content")
def set_content(message):
    try:
        if message["type"] == "push":
            api.set_content(message)
        elif message["type"] == "pull":
            data = api.get_content(message)
            content = {}
            if data:
                content["text"] = data
            emit("get_content", content)
    except Exception as e:
        print e

@socketio.on("set_activity")
def set_content(message):
    try:
        api.set_activity(message)
    except Exception as e:
        print e

if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0")
