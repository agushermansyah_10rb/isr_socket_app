# -*- coding: utf-8 -*-
from datetime import datetime, date
from decimal import Decimal

import redis
from core import config
from eblib.beanstalk import Pusher, Worker

__author__ = 'root'


def error_response(status_code, message):
    return build_result(False, message=message)


def init_redis(db=None):
    redis_host = config.load().get('redis', 'server_host')
    redis_port = config.load().get('redis', 'server_port')
    redis_pwd = config.load().get('redis', 'server_pwd')
    redis_db = db if db is not None else config.load().get('redis', 'server_db')
    redis_pointer = redis.StrictRedis(host=redis_host, port=int(redis_port), db=int(redis_db), password=redis_pwd)
    redis_pointer.config_set("save", "1 1")

    return redis_pointer


def init_pusher_beanstalk(conf, tube, section='beanstalk'):
    serverhost = conf.get(section, 'serverhost')
    try:
        serverport = int(conf.get(section, 'serverport'))
    except:
        serverport = 11300
    basetube = conf.get(section, 'tubename')
    tubename = "%s_%s" % (basetube, tube)
    return Pusher(tubename, serverhost, serverport)


def init_worker_beanstalk(conf, tube, worker_id='worker', section='beanstalk'):
    serverhost = conf.get(section, 'serverhost')
    basetube = conf.get(section, 'tubename')
    tubename = "%s_%s" % (basetube, tube)
    return Worker(tubename, worker_id, serverhost)


def json_build(data):
    if isinstance(data, dict):
        for k, v in data.iteritems():
            data[k] = json_build(v)

    if isinstance(data, list):
        for i, v in enumerate(data):
            data[i] = json_build(v)

    if isinstance(data, tuple):
        data = json_build([item for item in data])

    if isinstance(data, datetime):
        data = str(data)

    if isinstance(data, date):
        data = str(data)

    if isinstance(data, long):
        data = str(data)

    if isinstance(data, Decimal):
        data = str(data)

    return data


def build_result(status=False, data=None, message=None):
    if data:
        data = json_build(data)
    return {"status": status, "data": data, "message": message}
