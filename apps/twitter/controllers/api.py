from core import logger
from core.controller import Controller


class Api(Controller):
    def __init__(self):
        Controller.__init__(self)

    def get_content(self, message):
        try:
            user = str(message["username"]).lower()
            name = "isr_v3:content:{}".format(user)
            return self.redis_pointer.rpop(name)
        except Exception as e:
            raise e

    def set_content(self, message):
        try:
            user = str(message["username"]).lower()
            name = "isr_v3:content:{}".format(user)
            if "content" in message:
                self.redis_pointer.lpush(name, message["content"])
        except Exception as e:
            raise e

    def set_activity(self, message):
        try:
            name = "isr_v3:activity"
            value = str(message["username"]).lower()
            count = self.redis_pointer.hget(name, value)
            if count:
                new_count = int(count) + 1
                self.redis_pointer.hset(name, value, new_count)
            else:
                self.redis_pointer.hset(name, value, 1)
        except Exception as e:
            raise e

