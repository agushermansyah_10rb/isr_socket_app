from core import config
from helper.general import init_redis


class Controller:
    def __init__(self):
        self.config = config.load()
        self.redis_pointer = init_redis()